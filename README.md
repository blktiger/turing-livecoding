# turing

## Requirements

To run this project you'll need to do the following:

1. Install node.js
2. Execute `npm install`

## Build Instructions

Execute the tests with `grunt test`

Error check code with `grunt jshint`

Error check code and then execute tests with `grunt`

