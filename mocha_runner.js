var Mocha = require('../node_modules/grunt-mocha-test/node_modules/mocha');

var mocha = new Mocha();

var passed = [];
var failed = [];

mocha.addFile('turing.spec');

mocha.run(function(){

    console.log(passed.length + ' Tests Passed');
    passed.forEach(function(testName){
        console.log('Passed:', testName);
    });

    console.log("\n"+failed.length + ' Tests Failed');
    failed.forEach(function(testName){
        console.log('Failed:', testName);
    });

}).on('fail', function(test){
    failed.push(test.title);
}).on('pass', function(test){
    passed.push(test.title);
});