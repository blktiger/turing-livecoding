require('./turing');

(function (global) {
	'use strict';
	
	global.turing.functional = {
		// Call the provided callback on each element in the array.
		each: function (enumerable, callback) {
			for (var property in enumerable) {
				callback.apply(enumerable[property], [property, enumerable[property], enumerable]);
			}
		},
		
		// Return an array containing results for each object in the enumerable.
		map: function (enumerable, callback) {
			var array = [];
			for (var property in enumerable) {
				var result = callback(property, enumerable[property]);
				array.push(result);
			}
			return array;
		},
		
		// Return the first matching value.
		filter: function (enumerable, callback) {
			var array = [];
			for (var property in enumerable) {
				if (callback(property, enumerable[property])) {
					array.push(enumerable[property]);
				}
			}
			return array;
		},
		
		// Produce a single value from an array.
		reduce: function (enumerable, callback, memory) {
			for (var property in enumerable) {
				memory = callback(memory, enumerable[property]);
			}
			return memory;
		},
		
		// Set the 'this' context of a function. This is called proxy in jQuery.
		bind: function (fn, context) {
			return function () {
				return fn.apply(context, arguments);
			};
		}
	};
})(typeof window === 'undefined' ? global : window);