require('./turing');

(function (global) {
	'use strict';

	global.turing.oo = {
		extend: function (object, methods) {
			var i, property;

			for (i = 1; i < arguments.length; i++) {
				methods = arguments[i];
				for (property in methods) {
					if (methods.hasOwnProperty(property)) {
						object[property] = methods[property];
					}
				}
			}
		},

		Class: {
			create: function () {
				var parent, mixins, mixin, i;
				if (typeof arguments[0] === 'function') {
					parent = arguments[0];
					mixins = Array.prototype.slice.call(arguments, 1);
				} else {
					mixins = Array.prototype.slice.call(arguments, 0);
				}

				// Create the class
				var Class = function () {
					if (this.initialize) {
						this.initialize.apply(this, arguments);
					}
				};
				
				// Setup inheritance
				if (parent) {
					Class.prototype = Object.create(parent.prototype);
				}

				// Copy the provided methods to the class prototype
				for (i = 0; i < arguments.length; i++) {
					mixin = arguments[i];
					global.turing.oo.extend(Class.prototype, mixin);
				}
				
				// Setup $super and $parent
				if (parent) {
					Class.prototype.$parent = parent.prototype;
					Class.prototype.$super = function () {
						this.$parent[arguments[0]].apply(this, Array.prototype.slice.call(arguments, 1));
					};
				}
				
				return Class;
			}
		}
	};
})(typeof window === 'undefined' ? global : window);