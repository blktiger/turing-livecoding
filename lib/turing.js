(function (global) {
	'use strict';
	
	if (global.turing) {
		throw new Error('turing has already been defined');
	} else {
		global.turing = {
			VERSION : '0.0.1'
		};
	}
})(typeof window === 'undefined' ? global : window);