/* global turing */
require('./turing');
require('./turing.oo');
require('./turing.functional');
require('./turing.dom');

(function (global) {
	'use strict';
	
	global.turing.chain = {
		Chainable: turing.oo.Class.create({
			initialize: function (value) {
				this.value = value;
			},
			
			find: function (selector) {
				this.value = global.turing.dom.find(this.value, selector);
				return this;
			}
		})
	};
	
	global.turing.functional.each(global.turing.functional, function (methodName, method) {
		global.turing.chain.Chainable.prototype[methodName] = function () {
			var args = Array.prototype.slice.call(arguments);
			args.unshift(this.value);
			
			var result = method.apply(this, args);
			if (result) {
				this.value = result;
			}
			
			return this;
		};
	});
})(typeof window === 'undefined' ? global : window);