/* global turing */
require('./turing');
require('./turing.oo');
require('./turing.functional');

(function (global) {
	'use strict';
	
	global.turing.events = {
		add: function (target, type, handler) {
			if (target.addEventListener) {
				target.addEventListener(type, handler);
			}

			target.events = target.events || {};
			target.events[type] = target.events[type] || [];
			target.events[type].push(handler);
		},
		
		remove: function (target, type) {
			if (target.events && target.events[type]) {
				for (var i = 0; i < target.events[type].length; i++) {
					if (target.removeEventListener) {
						target.removeEventListener(type, target.events[type][i], false);
					}
				}
				target.events[type] = [];
			}
		},
		
		fire: function (target, type) {
			if (target.dispatchEvent) {
				var event = global.document.createEvent('HTMLEvent');
				event.eventName = type;
				event.initEvent(type, true, true);
				target.dispatchEvent(event);
			} else {
				for (var i = 0; i < target.events[type].length; i++) {
					target.events[type][i].apply(target, Array.prototype.slice.call(arguments, 2));
				}
			}
		}
	};
})(typeof window === 'undefined' ? global : window);