require('./turing');
require('./turing.oo');
require('./turing.events');
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

(function (global) {
	'use strict';
	
	var DONE = 4;
	
	global.turing.net = {
		ajax: function (options) {
			// Set default options
			options.async = options.async || true;
			options.body = options.body || '';
			options.crossDomain = options.crossDomain || false;
			options.headers = options.headers || {
				'Accept': 'text/javascript, text/html, application/xml, text/xml'
			};
			options.method = options.method || 'get';
			
			var request = new XMLHttpRequest();
			request.onreadystatechange = function (readyState) {
				if (request.readyState === DONE) {
					var isSuccess = request.status >= 200 && request.status < 300;
					var isNotModified = request.status === 304;
					var isUnknownStatusWithResponseText = request.status === 0 && request.responseText;
					
					if (isSuccess || isNotModified || isUnknownStatusWithResponseText) {
						global.turing.events.fire(request, 'success', request.responseText, request);
					} else {
						global.turing.events.fire(request, 'error', request);
					}
				}
			};
			request.open(options.method, options.url, options.async);
			request.withCredentials = options.crossDomain;
			for (var name in options.headers) {
				request.setRequestHeader(name, options.headers[name]);
			}
			request.send(options.body);
			
			return request;
		}
	};
})(typeof window === 'undefined' ? global : window);