require('./turing');
require('./turing.chain');

(function (global) {
	'use strict';
	
	global.turing.dom = {
		/**
		 * Find a list of dom elements by a css selector.
		 * 
		 * Supported browsers: IE8+, FF, Webkit
		 */
		find: function (element, selector) {
			var nodeList;
			
			if (!selector) {
				nodeList = global.document.querySelectorAll(element);
			} else {
				nodeList = element.querySelectorAll(selector);
			}
			
			return new global.turing.chain.Chainable(nodeList);
		},
		
		/**
		 * Execute some javascript code when the document is ready.
		 *
		 * Supported browsers: IE9+, FF, Webkit
		 */
		ready: function (callback) {
			document.addEventListener('DOMContentLoaded', callback);
		}
	};
})(typeof window === 'undefined' ? global : window);