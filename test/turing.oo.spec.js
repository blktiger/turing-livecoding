/* global turing, describe, it */
'use strict';

var expect = require('chai').expect;
require('../lib/turing.oo');

describe("prototype inheritance", function () {
	function Vector(x, y) {
		this.x = x;
		this.y = y;
	}
	
	Vector.prototype.toString = function () {
		return '(' + this.x + ',' + this.y + ')';
	};
	
	function Point(x, y, color) {
		Vector.call(this, x, y);
		this.color = color;
	}
	
	// Point.prototype = Object.create(Vector.prototype);
	Point.prototype = new Vector();
	Point.prototype.constructor = Point;
	
	describe("point", function () {
		it("initializes x, y, and color", function () {
			var point = new Point(1, 2, 'red');
			expect(point.x).to.be.equal(1);
			expect(point.y).to.be.equal(2);
			expect(point.color).to.be.equal('red');
		});
		
		it("calls parent methods", function () {
			var point = new Point(1, 2, 'red');
			expect(point.toString()).to.be.equal('(1,2)');
		});
	});
});

describe("turing.oo", function () {
	describe("extend", function () {
		it("copies properties", function () {
			var object = {};
			
			global.turing.oo.extend(object, {
				a: 1,
				b: 2
			}, {
				a: 3,
				c: 4
			});
			
			expect(object.a).to.be.equal(3);
			expect(object.b).to.be.equal(2);
			expect(object.c).to.be.equal(4);
		});
	});

	describe("Class", function () {
		var Vector = global.turing.oo.Class.create({
			initialize: function(x, y) {
				this.x = x;
				this.y = y;
			},
			
			toString: function () {
				return '(' + this.x + ',' + this.y + ')';
			}
		});
		
		describe("create", function () {
			it("calls the initialize method", function () {
				var vector = new Vector(1, 2);
				expect(vector.x).to.be.equal(1);
				expect(vector.y).to.be.equal(2);
			});
			
			it("mixes in other objects", function () {
				var mixin = {
					log: function () {},
					
					initialize: function () {
						this.value = 2
					}
				};
				
				var MyClass = global.turing.oo.Class.create({
					initialize: function () {
						this.value = 1;
					}
				}, mixin);
				
				var myInstance = new MyClass();
				expect(myInstance.value).to.be.equal(2);
				expect(myInstance.log).to.be.equal(mixin.log);
			});
		});
		
		describe("inheritance", function () {
			var Point = global.turing.oo.Class.create(Vector, {});
			
			it("correctly sets up inheritance", function () {
				var point = new Point(1, 2, 'red');
				expect(point instanceof Point).to.be.equal(true);
				expect(point instanceof Vector).to.be.equal(true);
			});
		});
		
		describe("$super", function () {
			var Point = global.turing.oo.Class.create(Vector, {
				initialize: function(x, y, color) {
					this.$super('initialize', x, y);
					this.color = color;
				}
			});
			
			it("runs the super initialize method", function () {
				var point = new Point(1, 2, 'red');
				expect(point.x).to.be.equal(1);
				expect(point.y).to.be.equal(2);
				expect(point.color).to.be.equal('red');
			});
		});
	});
});