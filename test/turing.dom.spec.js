/* global turing, describe, it */
'use strict';

var expect = require('chai').expect;
var jsdom = require('jsdom').jsdom;

require('../lib/turing.dom');

describe('turing.dom.find', function() {
	it('finds all elements in the dom', function() {
		global.document = jsdom('<html><body><div></div></body></html>');
		expect(turing.dom.find('body').value.length).to.be.equal(1);
	});
	
	it('finds all elements in a particular element', function() {
		global.document = jsdom('<html><body><div><div></div></div></body></html>');
		var body = turing.dom.find('body').value[0];
		expect(turing.dom.find(body, 'div').value.length).to.be.equal(2);
	});
});

describe('turing.dom.ready', function() {
	it('executes the ready function when DOMContentLoaded is called', function () {
		var isReady = false;
		global.document = jsdom('<html><body><div><div></div></div></body></html>');
		
		turing.dom.ready(function () {
			isReady = true;
		});
		
		expect(isReady).to.be.equal(false);
	});
});