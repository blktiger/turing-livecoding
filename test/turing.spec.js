/* global turing, describe, it */
'use strict';

var expect = require('chai').expect;

require('../lib/turing');

describe('turing', function() {
	it('should have a version', function() {
		expect('string').to.be.equal(typeof global.turing.VERSION);
	});
});
