/* global turing, describe, it */
'use strict';

var expect = require('chai').expect;
var jsdom = require('jsdom').jsdom;

require('../lib/turing.chain');

describe('turing.chain', function() {
	it('stores the value provided', function() {
		var array = [];
		var chainable = new global.turing.chain.Chainable(array);
		expect(chainable.value).to.be.equal(array);
	});
	
	it('Allows functional methods to be called in a chain', function() {
		var array = [1, 2, 3];
		var chainable = new global.turing.chain.Chainable(array);
		chainable.map(function (key, value) {
			return value + 1;
		}).reduce(function (memory, value) {
			return memory + value;
		}, 0);
		
		expect(chainable.value).to.be.equal(9);
	});
});