/* global turing, describe, it */
'use strict';

var expect = require('chai').expect;

require('../lib/turing.functional');

describe('functional.each', function () {
	it('should apply a callback function to all elements of an array', function () {
		var array = ["1", "2"];
		turing.functional.each(array, function (key, value) {
			array[key] = key + value;
		});
		
		expect(array[0]).to.be.equal("01");
		expect(array[1]).to.be.equal("12");
	});
	
	it('should set "this" to the element being iterated over', function () {
		var array = ["1", "2"];
		turing.functional.each(array, function (key) {
			array[key] = key + this;
		});
		
		expect(array[0]).to.be.equal("01");
		expect(array[1]).to.be.equal("12");
	});
});

describe('functional.map', function () {
	it('should return a new array of results', function () {
		var array = ["1", "2"];
		var result = turing.functional.map(array, function (key, value) {
			return key + value;
		});
		
		expect(array[0]).not.to.be.equal(result[0]);
		expect(array[1]).not.to.be.equal(result[1]);
		
		expect(result[0]).to.be.equal("01");
		expect(result[1]).to.be.equal("12");
	});
});

describe('functional.filter', function () {
	it ('should return a new array of filtered results', function () {
		function isEven(key, value) {
			return value % 2 === 0;
		}
		
		var array = [1, 2, 3, 4];
		
		var result = turing.functional.filter(array, isEven);
		
		expect(result.length).to.be.equal(2);
		expect(result[0]).to.be.equal(2);
		expect(result[1]).to.be.equal(4);
	});
});

describe('functional.reduce', function () {
	it ('reduces an array to a single value', function () {
		function sum(a, b) {
			return a + b;
		}
		
		var result = turing.functional.reduce([1, 2, 3], sum, 0);
		
		expect(result).to.be.equal(6);
	});
});

describe('functional.bind', function () {
	it ('calls the function with the provided context', function () {
		var greet = turing.functional.bind(function (greeting) {
			return greeting + ': ' + this.name
		}, {name: 'moe'});
		expect(greet('hi')).to.be.equal('hi: moe');
	});
});
