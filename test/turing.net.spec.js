/* global turing, describe, it */
'use strict';

var expect = require('chai').expect;

require('../lib/turing.net');
require('../lib/turing.events');

describe('turing.net.ajax', function() {
	it('handles a successful request', function() {
		var request = turing.net.ajax({
			url: 'http://www.garmin.com'
		});
		
		turing.events.add(request, 'success', function(request) {
			expect(request.status).to.be.equal(200);
		});
	});
	
	it('handles an error', function() {
		var request = turing.net.ajax({
			url: 'http://www.garmin.com/404'
		});
		
		turing.events.add(request, 'error', function(request) {
			expect(request.status).to.be.equal(404);
		});
	});
});