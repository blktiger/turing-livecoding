/* global turing, describe, it */
'use strict';

var expect = require('chai').expect;
var jsdom = require('jsdom').jsdom;

require('../lib/turing.dom');
require('../lib/turing.events');

describe('turing.dom.events', function () {
	it('adds an event and calls the handler when the event fires', function () {
		global.document = jsdom('<html><body><div></div></body></html>');
		
		var body = turing.dom.find('body').value[0];
		var wasClicked = false;
		var click = function() {
			wasClicked = true;
		};
		
		turing.events.add(body, 'click', click);
		
		turing.events.fire(body, 'click');
		
		expect(wasClicked).to.be.equal(true);
		expect(body.events['click'][0]).to.be.equal(click);
	});
	
	it('removes an event', function () {
		global.document = jsdom('<html><body><div></div></body></html>');
		
		var body = turing.dom.find('body').value[0];
		var wasClicked = false;
		
		turing.events.add(body, 'click', function() {
			wasClicked = true;
		});
		
		turing.events.remove(body, 'click');
		
		turing.events.fire(body, 'click');
		
		expect(wasClicked).to.be.equal(false);
		expect(body.events['click'][0]).to.be.equal(undefined);
	});
	
	it('fires events on non-DOM objects', function() {
		var object = {};
		var wasTested = false;
		var test = function() {
			wasTested = true;
		}
		
		turing.events.add(object, 'test', test);
		
		turing.events.fire(object, 'test');
		
		expect(wasTested).to.be.equal(true);
		expect(object.events['test'][0]).to.be.equal(test);
	});
});